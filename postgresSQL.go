package database

import (
	"database/sql"
	"fmt"
	"gitlab.com/modules-shortcut/go-utils"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"time"
)

type DATABASE struct {
	HOST string `env:"DB_HOST" envDefault:"localhost" json:"db_host"`
	PORT string `env:"DB_PORT" envDefault:"5432" json:"db_port"`
	NAME string `env:"DB_NAME" json:"db_name"`
	USER string `env:"DB_USER" json:"db_user"`
	PASS string `env:"DB_PASS" json:"db_pass"`
	SSL  string `env:"DB_SSL" envDefault:"disable" json:"db_ssl"`
	TZ   string `env:"TIME_ZONE" envDefault:"Asia/Jakarta" json:"db_tz"`

	MAX_IDLE_CONNS      int `env:"MAX_IDLE_CONNS" envDefault:"100" json:"max_idle_conns"`
	MAX_OPEN_CONNS      int `env:"MAX_OPEN_CONNS" envDefault:"1000" json:"max_open_conns"`
	MAX_IDLE_CONNS_TIME int `env:"MAX_IDLE_CONNS_TIME" envDefault:"60" json:"max_idle_conns_time"`
}

func (this *DATABASE) ParseDatabase() (err error) {
	return err
}

type PostgreSQL struct {
	Config DATABASE
}

func (pgsql *PostgreSQL) Connect(cfg *gorm.Config) (DB *gorm.DB, sql_ *sql.DB) {

	var (
		dsn string
		err error
	)

	// for change all the variable you can change in utils/database/config.go
	dsn = fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=%s TimeZone=%s",
		pgsql.Config.HOST,
		pgsql.Config.USER,
		pgsql.Config.PASS,
		pgsql.Config.NAME,
		pgsql.Config.PORT,
		pgsql.Config.SSL,
		pgsql.Config.TZ,
	)

	if cfg == nil {
		cfg = &gorm.Config{}
	}

	if DB, err = gorm.Open(postgres.Open(dsn), cfg); err != nil {
		utils.ErrorHandler(err)
		panic(err)
	}

	if sql_, err = DB.DB(); err != nil {
		utils.ErrorHandler(err)
		panic(err)
	}

	sql_.SetConnMaxIdleTime(time.Duration(pgsql.Config.MAX_IDLE_CONNS_TIME) * time.Minute)
	sql_.SetMaxIdleConns(pgsql.Config.MAX_IDLE_CONNS)
	sql_.SetMaxOpenConns(pgsql.Config.MAX_OPEN_CONNS)

	return
}
