package database

import (
	"database/sql"
	"gorm.io/gorm"
)

var (
	postgresSQL PostgreSQL
	DB          *gorm.DB
	DB_METHOD   *sql.DB
)

func init() {
	// call parse function for initial all variable for configuration database connection
	// you can see in config.go
	err := postgresSQL.Config.ParseDatabase()
	if err != nil {
		panic(err)
	}

	DB, DB_METHOD = ConnectionPostgresSQL()
}

func ConnectionPostgresSQL() (DB *gorm.DB, sql_ *sql.DB) {
	return postgresSQL.Connect(nil)
}
